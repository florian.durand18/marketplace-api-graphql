FROM node:15-alpine

RUN apk add --no-cache yarn
RUN apk add --no-cache python g++ make

WORKDIR /app

COPY ["package.json", "./"]

RUN yarn global add nodemon
RUN yarn global add prisma1
RUN yarn

CMD nodemon src/app.js
