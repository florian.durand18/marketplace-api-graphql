# MARKETPLACE-API-GRAPHQL

## Installation

- Install the dependencies

```bash
  npm install
```

## Run the for local development

```bash
  npm start
```

## Launch docker-compose for prisma

```bash
  docker-compose up
```

```bash
  docker-compose up -d
```
