const initialBetsFragment = `
fragment initialBetWithElements on InitialBet {
        id
        creator {
            id
            username
            email
        }
        product {
            id
            name
            price
        }
        bets {
            id
            bet
            createdAt
            better {
                username
            }
        }
        updatedAt
        createdAt
        currentBet
        endBetTime
    }
`

const Subscription = {
    productBets: {
        subscribe: async (root, { productId }, { prisma }, info) => {
            return await prisma.$subscribe
                .initialBet({
                    mutation_in: ['UPDATED'],
                    node: {
                        product: { id: productId },
                    },
                })
                .node()
                .$fragment(initialBetsFragment)
        },
        resolve: (obj) => obj,
    }
}

module.exports = Subscription
