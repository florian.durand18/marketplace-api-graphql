const initialBetsFragment = `
fragment initialBetWithElements on InitialBet {
        id
        creator {
            id
            username
            email
        }
        product {
            id
            name
            price
        }
        bet {
            bet
            createdAt
            better {
                username
            }
        }
        updatedAt
        createdAt
        currentBet
        endBetTime
    }
`


const BetsFragment = `
fragment BetWithElements on Bet {
        id
        better {
            id
            username
            email
        }
        InitialBet {
            id
            createdAt
            updatedAt
            currentBet
            endBetTime
        }
        bet
        createdAt
    }
`

const Query = {
    fetchUserBets(root, args, { prisma, claims: { id } }, info) {
        return prisma.user({ id }).bets().$fragment(BetsFragment)
    },
    fetchProductBets(root, { productId }, { prisma }, info) {
        return prisma.product({ id: productId }).initialBet().bets().$fragment(BetsFragment)
    },
    fetchUserInitialBets(root, args, { prisma, claims: { id } }, info) {
        return prisma.user({ id }).initialBets().$fragment(initialBetsFragment)
    },
    async fetchProductInitialBet(root, { productId }, { prisma }) {
        return prisma.product({ id: productId }).initialBet().$fragment(initialBetsFragment)
    },
}

module.exports = Query
