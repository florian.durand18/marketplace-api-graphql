const is = require('is_js')

const Mutation = {
    createInitialBet: async (root, args, { prisma, claims }) => {
        const { data: { endBetTime, currentBet, productId } } = args;
        const { id: userId } = claims

        try {
            if (await prisma.product({ id: productId }).initialBet()) throw new Error('Bet already initiated')
            return prisma.createInitialBet({
                currentBet,
                // Now + 2 days by default
                endBetTime: endBetTime ? new Date(endBetTime).toISOString() : new Date(Date.now() + 172800).toISOString(),
                creator: {
                    connect: { id: userId }
                },
                product: {
                    connect: { id: productId }
                }
            })
        } catch (e) {
            console.error(e)
            return e
        }
    },
    createUserBet: async (root, args, { prisma, claims, pubsub }) => {
        const { data: { productId, bet } } = args;
        const { id: userId } = claims

        try {
            const [initialBet] = await prisma.initialBets({
                where: {
                    product: { id: productId }
                }
            })
            if (is.null(initialBet)) throw new Error('No initial bet for this product')

            let error = null

            if (initialBet.currentBet >= bet) error = 'Bet not high enough. current bet : ' + initialBet.currentBet

            if (initialBet.endBetTime >= Date.now()) error = 'Betting session finished'

            if (error) throw new Error(error)
            const theBet = await prisma.createBet({
                initialBet: {
                    connect: { id: initialBet.id }
                },
                better: {
                    connect: { id: userId }
                },
                bet
            })
            await prisma.updateInitialBet({ data: { currentBet: bet }, where: { id: initialBet.id } })

            return theBet
        } catch (e) {
            return e
        }
    }
}

module.exports = Mutation
