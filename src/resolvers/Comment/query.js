const Query = {
    fetchUserComments: (root, args, { prisma, claims: { id } }, info) => {
        return prisma.user({ id }).comments()
    }
}