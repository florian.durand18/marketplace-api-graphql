const Mutation = {
    createComment: (root, args, { prisma, claims }, info) => {
        const { data: { content, commentId } } = args;
        const { id: userId } = claims
        try {
            return prisma.createComment({
                user: {
                    connect: { id: userId }
                },
                product: {
                    connect: { id: productId }
                },
                content
            });
        } catch (e) {
            return e
        }
    },
    updateComment: async (root, { data }, { prisma }, info) => {
        const { commentId } = data
        delete data.commentId
        try {
            return prisma.updateComment({ where: { id: commentId }, data })
        } catch (e) {
            return e
        }
    },
    deleteComment: async (root, { data }, { prisma }, info) => {
        const { commentId } = data

        try {
            return prisma.deleteComment({ id: commentId })
        } catch (e) {
            return e
        }
    }
}

module.exports = Mutation
