const Subscription = {
    products: {
        subscribe: async (root, args, { prisma }, info) => {
            return await prisma.$subscribe
                .product({
                    mutation_in: ['CREATED'],
                })
                .node()
        },
        resolve: (obj) => obj,
    }
}

module.exports = Subscription
