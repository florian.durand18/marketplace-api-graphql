const Mutation = {
    createProduct: (root, args, { prisma, claims }, info) => {
        const { data: { name, description, price } } = args;
        const { id: userId } = claims
        try {
            return prisma.createProduct({
                user: {
                    connect: { id: userId }
                },
                name,
                description,
                price,
            });
        } catch (e) {
            return e
        }
    },
    updateProduct: async (root, { data }, { prisma }, info) => {
        const { productId } = data
        delete data.productId
        try {
            return prisma.updateProduct({ where: { id: productId }, data })
        } catch (e) {
            return e
        }
    },
    deleteProduct: async (root, { data }, { prisma }, info) => {
        const { productId } = data
        try {
            return prisma.deleteProduct({ id: productId })
        } catch (e) {
            return e
        }
    }
}

module.exports = Mutation