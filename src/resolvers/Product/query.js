const Query = {
    fetchUserProducts: (root, args, { prisma, claims: { id } }, info) => {
        return prisma.user({ id }).products()
    }
}

module.exports = Query