const betResolvers = require("./Bet")
const userResolvers = require("./User")
const productResolvers = require("./Product")
const adminResolvers = require("./Admin")

const resolvers = {
  Query: {
    ...betResolvers.Query,
    ...productResolvers.Query,
    ...userResolvers.Query,
    ...adminResolvers.Query
  },
  Mutation: {
    ...betResolvers.Mutation,
    ...productResolvers.Mutation,
    ...userResolvers.Mutation,
  },
  Subscription: {
    ...betResolvers.Subscription,
    ...productResolvers.Subscription,
  }
}

module.exports = resolvers
