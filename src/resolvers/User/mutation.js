const bcrypt = require('bcrypt');
const { signToken } = require('../../permissions');

const Mutation = {
    register: async (root, args, { prisma }, info) => {
        const { data: { email, username, password } } = args;
        try {
            const newUser = await prisma.createUser({
                email,
                username,
                password: bcrypt.hashSync(password, 3),
            });
            return { token: signToken(newUser) };
        } catch (e) {
            return e
        }
    },
    registerVendor: async (root, args, { prisma }, info) => {
        const { data: { email, username, password } } = args;
        try {
            const newUser = await prisma.createUser({
                email,
                username,
                password: bcrypt.hashSync(password, 3),
                role: 1
            });
            return { token: signToken(newUser) };
        } catch (e) {
            return e
        }
    },
    updateUser: async (root, { data }, { prisma, claims: { id } }, info) => {
        try {
            if (is.string(data.password)) data.password = bcrypt.hashSync(password, 3)

            return prisma.updateUser({ where: { id }, data })
        } catch (e) {
            return e
        }
    },
    deleteUser: async (root, args, { prisma, claims: { id } }, info) => {
        try {
            return prisma.deleteUser({ id })
        } catch (e) {
            return e
        }
    }
}

module.exports = Mutation