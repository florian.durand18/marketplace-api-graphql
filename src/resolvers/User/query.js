const bcrypt = require('bcrypt');
const { signToken } = require('../../permissions');

const Query = {
    login: async (root, args, { prisma }, info) => {
        const { data: { email, password, username } } = args;
        try {
            const [theUser] = await prisma.users({
                where: {
                    OR: [
                        { email },
                        { username }
                    ]
                }
            })
            if (!theUser) throw new Error('User not found');
            const isMatch = bcrypt.compareSync(password, theUser.password);
            if (!isMatch) throw new Error('Wrong password');
            return { token: signToken(theUser) }
        } catch (e) {
            return e
        }
    },
}

module.exports = Query