const Query = {
    fetchUsers: async (root, args, { prisma }, info) => prisma.users(),
    fetchProducts: async (root, args, { prisma }, info) => prisma.products(),
    fetchComments: async (root, args, { prisma }, info) => prisma.comments(),
    fetchInitialBets: async (root, args, { prisma }, info) => prisma.initalBets(),
    fetchBets: async (root, args, { prisma }, info) => prisma.bets(),
}

module.exports = Query