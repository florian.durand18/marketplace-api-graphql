const { rule, shield, allow, and } = require('graphql-shield')
const jwt = require('jsonwebtoken')
const is = require('is_js')

// Auth
const getClaims = (req) => {
    try {
        const token = jwt.verify(req.request.get("Authorization"), process.env.JWT_SECRET || "secret");
        if (is.undefined(token.id) || is.not.number(token.role)) throw new Error("Invalid payload")
        return token;
    } catch (e) {
        return e;
    }
}

// Rules

const isAuthenticated = rule()(async (parent, args, ctx, info) => {
    return ctx.claims !== null;
})

const isAdmin = rule()(async (parent, args, ctx, info) => {
    return ctx.claims >= 2;
})
const isVendor = rule()(async (parent, args, ctx, info) => {
    return ctx.claims.role >= 1;
})

// Permissions

const permissions = shield({
    Query: {
        '*': isAuthenticated,
    },
    Mutation: {
        '*': isAdmin,
        register: allow,
        createProduct: and(isAuthenticated, isVendor),
        createInitialBet: and(isAuthenticated, isVendor),
        createComment: isAuthenticated,
        createUserBet: isAuthenticated,
    },
    Subscription: {
        '*': isAuthenticated,
    }
},
    { fallbackRule: allow, allowExternalErrors: true })


const signToken = ({ id, role }) => jwt.sign({ id, role }, process.env.JWT_SECRET || "secret")

module.exports = {
    getClaims,
    permissions,
    signToken
};