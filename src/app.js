const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./generated/prisma-client')
const { permissions, getClaims } = require('./permissions')
const resolvers = require('./resolvers')


const server = new GraphQLServer({
  typeDefs: 'src/schema.graphql',
  resolvers,
  middlewares: [permissions],
  context: req => ({
    claims: getClaims(req),
    prisma
  }),
})

server.start(
  () => console.log(`GraphQL server is running on http://localhost:4000`)
)