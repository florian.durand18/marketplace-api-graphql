const jwt = require('jsonwebtoken');

const decodedToken = (req, requireAuth = true) => {
    const header = req.headers.authorization;

    if (header) {
        const token = header.replace('Bearer ', '');
        const decoded = jwt.verify(token, process.env.JWT_SECRET || 'secret');
        return decoded;
    }
    if (requireAuth) {
        throw new Error('Log in to access resource');
    }
    return null
}
module.exports = { decodedToken }